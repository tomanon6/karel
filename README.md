## názvy v kódu

RC - robot coordinates - proměnná obsahující souřadnice v systému robota (x, y, z)

RC2 - robot coordinates 2 - proměnná obsahující souřadnice v systému robota (x, y)

CC - camera coordinates - proměnná obsahující souřadnice v systému kamery (x, y)

RP - robot position - proměnná obsahující souřadnice v systému robota a úhly natočení gripperu (x, y, z, alpha, beta, gamma)
