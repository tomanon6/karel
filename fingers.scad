$fn = 50;

base_x = 25.4;
base_y = 25.4;
base_z = 6.35;
base_d = 4;

platfrom_border = 5;
platform_thickness = 5;
pb_gap = 0.5;

holder_length = 30;
holder_width = 15;
holder_dist_z = 20;
holder_dist_y = 10;
holder_thickness = 3;

dist_of_bases = 60;

translate([0, 0, base_y*0.8 + holder_dist_y + holder_length])
rotate([-90, 0, 0])
{
    %base();
    holder(holder_length, holder_width, holder_thickness,
        holder_dist_y, holder_dist_z, false);
}

translate([base_x, dist_of_bases, base_y*0.8 + holder_dist_y + holder_length])
rotate([-90, 0, 180])
{
    %base();
    holder(holder_length, holder_width, holder_thickness,
        holder_dist_y, holder_dist_z, true);
}

module holder(L_length, L_width, L_thick, L_dist_y, L_dist_z, in)
{
    platform(platfrom_border, platform_thickness, pb_gap);
    translate([base_x/2, base_y/2, platform_thickness/2])
    {
        difference()
        {
            hull()
            {
                translate([0, base_y*0.15, 0])
                    cube([20, 14, 1], center=true);
                translate([0, L_dist_y+2, L_dist_z])
                {
                    translate([0, 0, in ? L_thick : 0])
                        rotate([0, in ? 180 : 0, 0])
                            translate([0, 0, in ? -L_thick : 0])
                                L_part(8, 10, 1);
                }
            }
            translate([0, 0, -5])
                cylinder(r=base_d, h=20);
            translate([0, L_dist_y, L_dist_z+L_thick])
                L_part(10, 15, 6);
        }
        translate([0, L_dist_y, L_dist_z])
        {
            translate([0, 0, in ? L_thick : 0])
                rotate([0, in ? 180 : 0, 0])
                    translate([0, 0, in ? -L_thick : 0])
                        L_part(L_width, L_length, L_thick);
        }
    }
}

module platform(border, thickness, base_gap)
{
    difference()
    {
        translate([-border, 5, -base_z/2])
        {
            cube([base_x+2*border, base_y-5, base_z/2+thickness]);
        }
        translate([-base_gap, -base_gap, -base_gap])
        {
            minkowski()
            {
                hull()
                {
                    base();
                }
                cube(2*base_gap);
            }
        }
        translate([base_x/2, base_y/2, -1])
        {
            cylinder(r=base_d/2+0.5, h=thickness+2);
        }
    }   
}

module L_part(width, length, thickness)
{
    rotate([0, -45, 0])
    {
        cube([width+thickness, length, thickness]);
    }
    rotate([0, 45, 0])
    {
        translate([-width-thickness, 0, 0])
        {
            cube([width+thickness, length, thickness]);
        }
    }
}

module base()
{
    translate([0, 0, -base_z])
    {
        difference()
        {
            cube([base_x, base_y, base_z]);
            translate([base_x/2, base_y/2, -1])
            {
                cylinder(r=base_d/2, h=base_z+2);
            }
        }
    }
}