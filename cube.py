import numpy as np
import numpy.linalg as lin
import numpy.matlib as ml

class Cube:
    wall_thick = 1.8
    cube_sizes = np.array([[59.5, 59.5, 59.5],
                           [54.5, 54.5, 57.7],
                           [49.5, 49.5, 55.5],
                           [44.5, 44.5, 53.5],
                           [39.5, 39.5, 51.8],
                           [34.5, 34.5, 49.6],
                           [29.5, 29.5, 47.6]])

    def __init__(self, cube_index):
        self.index = cube_index
        self.center_top = np.array([0, 0, Cube.cube_sizes[self.index][2]])
        self.angle_z = 0

    def get_size(self):
        return Cube.cube_sizes[self.index]

    def get_corners(self):
        size = self.get_size()
        zero_center = np.array([[size[0]/2, size[1]/2, 0],
                                [-size[0]/2, size[1]/2, 0],
                                [-size[0]/2, -size[1]/2, 0],
                                [size[0]/2, -size[1]/2, 0]])
        rot_mat = get_rotation_z(self.angle_z)
        trans_mat = ml.repmat(self.center_top, 4, 1).T
        return rot_mat @ zero_center.T + trans_mat

    def move(self, pos_RC: np.array, angle: float, relative=False):
        self.center_top = self.center_top + pos_RC if relative else pos_RC
        self.angle_z = self.angle_z + angle if relative else angle

    def get_grab_corner(self):
        corners = self.get_corners()
        return corners.T[np.argmin(corners[0])]

    def __get_z_rot_for_cube(self):
        position = self.get_grab_corner()
        vectors = self.get_corners() - ml.repmat(position, 4, 1).T
        dist = lin.norm(vectors, axis=0)
        vector_to_opposite = vectors.T[np.argmax(dist)]
        return np.rad2deg(np.arctan2(vector_to_opposite[0], vector_to_opposite[1]))

    def get_RP_for_grab_cube(self, y_rot = 90):
        position = self.get_grab_corner()
        z_rot = self.__get_z_rot_for_cube()
        return np.concatenate((position-np.array([0, 0, 15]), np.array([0, y_rot, z_rot])))

def get_rotation_z(alpha):
    return np.array([[np.cos(alpha), -np.sin(alpha), 0],
                     [np.sin(alpha), np.cos(alpha), 0],
                     [0, 0, 1]])
