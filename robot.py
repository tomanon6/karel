import sys
sys.path.append(".")
sys.path.append("./hwControl")
from hwControl.robotCRS import robCRS93
from hwControl.CRS_commander import Commander
from hwControl.robCRSgripper import robCRSgripper
from hwControl.robCRSikt import robCRSikt
import numpy as np
from cube import Cube

class Robot:
    def __init__(self, init):
        self.rob = robCRS93()
        self.comm = Commander(self.rob)
        self.comm.open_comm('/dev/ttyUSB0')
        if init:
            self.comm.init()
            
    def get_ikt(self, pos):
        all_ikt = robCRSikt(self.rob, pos)
        sel_ikt = None
        for ikt in all_ikt:
            if ikt[0] < 90 and ikt[0] > -90 and ikt[1] > -90 and ikt[3] > -90 and ikt[3] < 90:
                sel_ikt = ikt
        return sel_ikt

    def goto(self, pos, force=False):
        pos = list(pos)
        if pos[2] < 0 and not force:
            raise Exception(f"z-coordinate ({pos[2]}) is under table")
        pos[2] = pos[2] + 40
        sel_ikt = self.get_ikt(pos)
        joints = self.comm.anglestoirc(sel_ikt)
        self.comm.move_to_pos(joints)
        self.comm.wait_ready()

    def gripper(self, close):
        strength = 0.34 * close - 0.3
        robCRSgripper(self.comm, strength)
        self.comm.wait_gripper_ready()

    def end(self):
        self.comm.rcon.close()
        
    def set_position_for_taking_images(self):
        self.comm.soft_home()
        irc = self.comm.axis_get_pos()[1]
        irc[2] = -40000
        self.comm.move_to_pos(irc)
        self.comm.wait_ready()
        
    def grab(self, cube: Cube):
        assert(abs(cube.center_top[2] - cube.get_size()[2]) < 1e-9)
        cube_RP = cube.get_RP_for_grab_cube()
        cube.move(np.array([0, 0, Cube.cube_sizes[0][2] + 20]), 0, True)
        above_RP = cube.get_RP_for_grab_cube()
        self.goto(above_RP)
        self.gripper(0.5)
        self.goto(cube_RP)
        self.gripper(1)
        self.goto(above_RP)

    def place(self, cube: Cube):
        cube_RP = cube.get_RP_for_grab_cube()
        dz = cube.get_size()[2] - cube.center_top[2]
        cube.move(np.array([0, 0, dz]), 0, True)
        assert(abs(cube.center_top[2] - cube.get_size()[2]) < 1e-9)
        placed_RP = cube.get_RP_for_grab_cube()
        self.goto(cube_RP)
        self.goto(placed_RP)
        self.gripper(0.5)
        self.goto(cube_RP)

    def move(self, cube: Cube, pos_RC: np.array, angle: float, relative=False):
        cube.move(pos_RC, angle, relative)
        cube_RP = cube.get_RP_for_grab_cube()
        assert(cube.center_top[2] - cube.get_size()[2] > -1e-9)
        self.goto(cube_RP)
        
    def put_in(self, smaller_cube: Cube, bigger_cube: Cube):
        assert(abs(smaller_cube.center_top[2] - smaller_cube.get_size()[2]) < 1e-9)
        self.grab(smaller_cube)
        self.move(smaller_cube, bigger_cube.center_top + np.array([-60, 60, smaller_cube.get_size()[2] + 10]), bigger_cube.angle_z)
        self.place(smaller_cube)
        self.grab(smaller_cube)
        self.move(smaller_cube, bigger_cube.center_top + np.array([0, 0, smaller_cube.get_size()[2] + 10]), bigger_cube.angle_z)
        tilt_angle = 5
        tilted_RP = smaller_cube.get_RP_for_grab_cube(90 - tilt_angle)
        tilted_RP[0] -= np.tan(np.deg2rad(tilt_angle)) * (35 + smaller_cube.get_size()[2])
        self.goto(tilted_RP)
        lower_tilted_RP = tilted_RP.copy()
        lower_tilted_RP[2] -= 12.5 + (1 - np.cos(np.deg2rad(tilt_angle))) * (35 + smaller_cube.get_size()[2])
        self.goto(lower_tilted_RP)
        self.move(smaller_cube, bigger_cube.center_top + np.array([-10, 0, smaller_cube.get_size()[2] - 2]), bigger_cube.angle_z)
        self.move(smaller_cube, bigger_cube.center_top + np.array([-10, 0, 25]), bigger_cube.angle_z)
        self.move(smaller_cube, bigger_cube.center_top + np.array([0, 0, 25]), bigger_cube.angle_z)
        self.gripper(0.5)
        smaller_cube.move(np.array([0, 0, -25]), 0, True)
        assert(np.linalg.norm(bigger_cube.center_top - smaller_cube.center_top) < 1e-9)
        assert(abs(bigger_cube.angle_z - smaller_cube.angle_z) < 1e-9)
