import cv2
import matplotlib.pyplot as plt
from recognition import get_verts_from_image
from timeit import default_timer as timer

if __name__ == '__main__':
    image = cv2.imread('vision/cubes.png')
    background = cv2.imread('vision/background.png')
    
    start = timer()
    all_verts = get_verts_from_image(image, background)
    end = timer()
    print(f"recognition takes {1000 * (end - start)} ms")
    
    final = image.copy()
    for verts in all_verts:
        for vert in verts:
            cv2.circle(final, (round(vert[0]), round(vert[1])), 5, (0, 255, 0), -1)
    
    plt.imshow(final)
    plt.title("final")
    plt.show()
