import PyCapture2
import numpy as np
import cv2
import time

class Camera:
    def __init__(self):
        bus = PyCapture2.BusManager()
        self.camera = PyCapture2.Camera()
        self.camera.connect(bus.getCameraFromIndex(0))
        self.camera.startCapture()
        self.camera.setProperty(type=PyCapture2.PROPERTY_TYPE.GAMMA, absValue=0.5)
        self.camera.setProperty(type=PyCapture2.PROPERTY_TYPE.SHUTTER, absValue=66)
    
    def get_image(self):
        self.camera.setProperty(type=PyCapture2.PROPERTY_TYPE.GAIN, absValue=159)
        time.sleep(0.5)
        buff = self.camera.retrieveBuffer()
        rgb = buff.convert(PyCapture2.PIXEL_FORMAT.RGB8)
        center_image = np.array(rgb.getData(), dtype="uint8").reshape((rgb.getRows(), rgb.getCols(), 3))
        self.camera.setProperty(type=PyCapture2.PROPERTY_TYPE.GAIN, absValue=163)
        time.sleep(0.5)
        buff = self.camera.retrieveBuffer()
        rgb = buff.convert(PyCapture2.PIXEL_FORMAT.RGB8)
        border_image = np.array(rgb.getData(), dtype="uint8").reshape((rgb.getRows(), rgb.getCols(), 3))
        blended = special_blend(center_image, border_image)
        return cv2.cvtColor(blended, cv2.COLOR_RGB2BGR)
    
def special_blend(center_image: np.array, border_image: np.array):
    mask = np.zeros_like(center_image, dtype='float')
    for i in range(mask.shape[0]):
        for j in range(mask.shape[1]):
            distance = ((i - mask.shape[0]/2)**2 + (j - mask.shape[1]/2)**2)**0.5
            mask[i, j] = distance / ((mask.shape[0]/2)**2 + (mask.shape[1]/2)**2)**0.5
    blended = center_image * np.clip(1 - mask, 0, 1) + border_image * np.clip(mask, 0, 1)
    return blended.astype(np.uint8)
