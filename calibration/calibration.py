import sys
sys.path.append('.')
import numpy as np
import numpy.matlib as ml
import numpy.linalg as lin
import cv2
from camera import Camera
from robot import Robot
from cube import Cube
from vision.recognition import get_verts_from_image
from scipy.optimize import minimize

def get_polygon_area(verts: np.array):
    acc = 0
    for i in range(verts.shape[1]):
        A = np.array([verts.T[i-1], verts.T[i]])
        acc += lin.det(A)
    return abs(acc / 2)

def fit_cubes_to_verts(all_verts):
    cubes = []
    A = np.load('calibration/A.npy')
    b = np.load('calibration/b.npy')
    all_verts = sorted(all_verts, key=lambda x: -get_polygon_area(get_RC2_from_CC(np.array(x).T, A, b)))
    for verts_CC, index in zip(all_verts, range(0, 5)):
        verts_RC2 = get_RC2_from_CC(np.array(verts_CC).T, A, b)
        cube = Cube(index)
        center = np.mean(verts_RC2, axis=1)
        vect = verts_RC2.T[0] - center
        angle = np.arctan2(vect[1], vect[0]) - np.pi / 4
        cube.move(np.concatenate((center, np.zeros(1))), angle, True)
        cubes.append(cube)
    return sorted(cubes, key=lambda x: x.index)

def get_RC2_from_CC(CC: np.array, A: np.array, b: np.array):
    return A @ CC + ml.repmat(b, CC.shape[1], 1).T

def calibration_loss(CC: np.array, RC2: np.array, x):
    A = np.array([[x[0], x[1]], [x[2], x[3]]])
    b = np.array([[x[4], x[5]]])
    calc_RC2 = get_RC2_from_CC(CC, A, b)
    diff_RC2 = calc_RC2 - RC2
    return sum(lin.norm(diff_RC2, axis=0)**2)

def calibrate(CC: np.array, RC2: np.array):
    x0 = [1, 0, 0, 1, 0, 0]
    result = minimize(lambda x: calibration_loss(CC.T, RC2.T, x), x0)
    x = result.x
    A = np.array([[x[0], x[1]], [x[2], x[3]]])
    b = np.array([x[4], x[5]])
    return A, b

if __name__ == '__main__':
    camera = Camera()
    robot = Robot(True)
    cube = Cube(3)
    z = cube.get_size()[2]

    calib_pos_RC = [[400, -200, z],
                    [500, -200, z],
                    [550, -200, z],
                    [400, -100, z],
                    [500, -100, z],
                    [550, -100, z],
                    [400, 0, z],
                    [500, 0, z],
                    [550, 0, z],
                    [400, 100, z],
                    [500, 100, z],
                    [550, 100, z]]

    robot.set_position_for_taking_images()
    background = camera.get_image()
    cv2.imwrite('vision/background.png', background)

    RC2 = []
    CC = []
    
    for pos_RC in calib_pos_RC:
        next_above = pos_RC + np.array([0, 0, 60])
        robot.move(cube, next_above, np.pi/4)
        robot.place(cube)
        robot.set_position_for_taking_images()
        img = camera.get_image()
        verts = get_verts_from_image(img, background)[0]
        vert = max(verts, key=lambda x: x[0])
        RC2.append(cube.get_grab_corner()[0:2])
        CC.append(vert)
        robot.grab(cube)

    A, b = calibrate(np.array(CC), np.array(RC2))
    np.save('calibration/A.npy', A)
    np.save('calibration/b.npy', b)

    robot.end()
    print('done')
