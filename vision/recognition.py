import cv2
import numpy as np
from scipy.optimize import minimize
from shapely.geometry import Point, Polygon

# https://stackoverflow.com/questions/54442057/calculate-the-euclidian-distance-between-an-array-of-points-to-a-line-segment-in/54442561#54442561
def lineseg_dists(p, a, b):
    p = np.atleast_2d(p)
    d = np.divide(b - a, np.linalg.norm(b - a))
    s = np.dot(a - p, d)
    t = np.dot(p - b, d)
    h = np.maximum.reduce([s, t, np.zeros(len(p))])
    c = np.cross(p - a, d)
    return np.hypot(h, c)

def square_dists(points, center_x, center_y, edge_size, angle):
    square = (center_x, center_y), (edge_size, edge_size), angle
    verts = cv2.boxPoints(square)
    mindist = np.ones(points.shape[0]) * np.inf
    for i in range(4):
        dist = lineseg_dists(points, verts[i], verts[(i+1)%4])
        mindist = np.minimum(mindist, dist)
    return mindist

def square_loss(points, square):
    dists = square_dists(points, square[0], square[1], square[2], square[3])
    return sum(dists ** 2)

def fit_square(contour):
    points = np.squeeze(contour)
    (center_x0, center_y0), (w, h), angle0 = cv2.minAreaRect(contour)
    edge_size0 = min(w, h)
    square0 = np.array([center_x0, center_y0, edge_size0, angle0])
    result = minimize(lambda sq: square_loss(points, sq), square0)
    square = (result.x[0], result.x[1]), (result.x[2], result.x[2]), result.x[3]
    verts = cv2.boxPoints(square)
    loss = square_loss(points, result.x)
    return verts, loss

def get_verts_from_image(image, background):
    all_verts = []
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    gray_background = cv2.cvtColor(background, cv2.COLOR_RGB2GRAY)
    diff_image = cv2.subtract(gray_background, gray_image)
    _, thresh_image = cv2.threshold(diff_image, 90, 255, 0)
    contours, hierarchy = cv2.findContours(thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    filtered_contours = [contour for contour in contours if cv2.contourArea(contour) > 800]
    sorted_contours = sorted(filtered_contours, key=cv2.contourArea, reverse=True)[0:10]
    for contour in sorted_contours:
        verts, loss = fit_square(contour)   # loss for future use
        add = True
        for bigger_verts in all_verts:
            poly = Polygon(bigger_verts)
            for vert in verts:
                point = Point(vert)
                if poly.contains(point):
                    add = False
        if add:
            all_verts.append(verts)
    return all_verts
