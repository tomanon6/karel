import sys
sys.path.append(".")
sys.path.append("./hwControl")
sys.path.append("./vision")
import numpy as np
from numpy import matlib as ml
from robot import Robot
from camera import Camera
import time
from matplotlib import pyplot as plt
from vision.recognition import get_verts_from_image, square_dists
from calibration.calibration import fit_cubes_to_verts, get_RC2_from_CC
import cv2
from cube import Cube
from shapely.geometry import Point, Polygon

def collide(area, vert):
    point = Point(vert.tolist())
    polygon = Polygon(area.T.tolist())
    return polygon.contains(point)

def is_place_area_occupied(pos, angle, all_verts):
    rect = np.array([[0, 0], [0, 165], [80, 165], [80, 0]]).T
    trans_rect = rect - ml.repmat(np.array([40, 40]), 4, 1).T
    rot_mat = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    rot_rect = rot_mat @ trans_rect
    area = Polygon((rot_rect.T + ml.repmat(np.array(pos), 4, 1)).tolist())
    A = np.load('calibration/A.npy')
    b = np.load('calibration/b.npy')
    for verts in all_verts:
        verts_RC2 = get_RC2_from_CC(verts.T, A, b).T
        bounding_box = Polygon(verts_RC2.tolist())
        if area.intersects(bounding_box):
            return True
    return False

def cubes_dist(cube1: Cube, cube2: Cube):
    corners = cube2.get_corners()
    dist = square_dists(corners[0:2].T, cube1.center_top[0], cube1.center_top[1], cube1.get_size()[0], cube1.angle_z)
    return min(dist)

def is_cubes_too_close(cubes):
    for i in range(len(cubes)):
        for j in range(i+1, len(cubes)):
            if cubes_dist(cubes[i], cubes[j]) < 20:
                return True
    return False

if __name__ == '__main__':
    camera = Camera()
    robot = Robot(True)

    robot.set_position_for_taking_images()
    image = camera.get_image()
    background = cv2.imread('vision/background.png')
    verts = get_verts_from_image(image, background)
    cubes = fit_cubes_to_verts(verts)
    
    target_pos = [500, 0]
    target_angle = np.pi / 4

    if is_cubes_too_close(cubes):
        print('error: distance between cubes is smaller than 20 mm')
        exit()

    if is_place_area_occupied(target_pos, target_angle, verts):
        print('error: place area is occupied')
        exit()
        
    warning = False
    area = np.array([[350, -200], [600, -200], [600, 150], [350, 150]]).T
    for cube in cubes:
        cube_RP = cube.get_RP_for_grab_cube()
        try:
            robot.get_ikt(cube_RP)
        except:
            print(f'error: ikt for cube with index {cube.index} not found')
            
        corners = cube.get_corners()
        for corner in corners.T.tolist():
            if not collide(area, np.array(corner[0:2])):
                warning = True
                print(f'warning: cube with index {cube.index} is outside of working area')
                
    if warning:
        ans = input('Do you want to ignore warnings and continue? Program may not function properly. (y/n) ')
        if ans != 'y' and ans != 'yes':
            robot.end()
            exit()

    last_cube = None
    for cube in cubes:
        if last_cube == None:
            robot.grab(cube)
            robot.move(cube, np.array([target_pos[0], target_pos[1], cube.center_top[2]]), target_angle)
            robot.place(cube)
        else:
            robot.put_in(cube, last_cube)
        last_cube = cube

    robot.goto([500, 0, 150, 0, 90, 0])
    print('done!')

    robot.end()
