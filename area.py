import sys
sys.path.append(".")
sys.path.append("./hwControl")
from hwControl.robotCRS import robCRS93
from hwControl.robCRSikt import robCRSikt
from matplotlib import pyplot as plt
import numpy as np

if __name__ == '__main__':
    robot = robCRS93()

    for x in range(200, 800, 25):
        for y in range(-500, 500, 25):
            all_ikt = robCRSikt(robot, [x, y, 40, 0, 90, 0])
            if np.isnan(all_ikt[2]).all():
                plt.scatter(x, y, c=[[1, 0, 0]], s=1)
            else:
                plt.scatter(x, y, c=[[0, 1, 0]], s=1)

    plt.axis('equal')
    plt.show()
